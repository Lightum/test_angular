//poner en true para trabajar sobre el navegador y en false para compilar
var debug = true;

angular.module('app', ['onsen'])
var app = angular.module('app')
app.controller('AppController', function($scope, $http, $window) {


    var size = new Object();
    var tabla_Personas = new LDB.Collection('Personas');
    $scope.campos = [];


    tabla_Personas.find({ __collection: "Personas" }, function(results) {

        if (results[0]) {

            size = Object.keys(results).length;
            //console.log(size);
            for (i = 0; i <= size - 1; i++) {
                $scope.campos.push(results[i]);
            }

        } else {
            //alert("No existen registros, consumiendo randomapi.com")

            var url = "https://randomapi.com/api/44519f8982fc33d740acc4034ac580ea";
            //console.log(url);
            $http.get(url)
                .success(function(data) {
                    //showModal();
                    //modal_wait.show();
                    tabla_Personas.save(data.results, function(_data) {
                        console.log('New items:', data.results);
                    });

                    $window.location.reload();
                })
                .error(function(err) {
                    console.log("Error al consumir randomapi.com");
                    alert("Error al consumir randomapi.com")
                    console.log(data);
                    // alert(data);
                });
        }

    });

    $scope.guardar = function() {
        var archivo;
        //var options = new Object();
        var tabla_Personas = new LDB.Collection('Personas');
        var tabla_Temporales = new LDB.Collection('Temporales');


        tabla_Temporales.find({ __collection: "Temporales" }, function(results) {
            if (results[0]) {
                archivo = results[0].Image;
            }
        });

        var item = {
            nombre: $scope.nombre,
            archivo: archivo
        };

        tabla_Personas.save(item, function(_item) {
            console.log('New items:', item);
        });

        $window.location.reload();

    };

    $scope.uploadFile = function(element) {
        var tabla_Temporales = new LDB.Collection('Temporales');
        var data = new FormData();
        data.append('file', $(element)[0].files[0]);
        //data.append('hola', 'ok');
        //console.log(data)
        jQuery.ajax({
            url: 'trata_imagen.php',
            type: 'post',
            data: data,
            contentType: false,
            processData: false,
            success: function(response) {
                tabla_Temporales.drop();

                var item = {
                    Image: response
                };

                tabla_Temporales.save(item, function(_item) {
                    console.log('New items:', item);
                });

                //console.log(response);
            },
            error: function(jqXHR, textStatus, errorMessage) {
                alert('Error uploading: ' + errorMessage);
            }
        });
    };

    $scope.desdeApi = function(element) {
        var url = "https://randomapi.com/api/44519f8982fc33d740acc4034ac580ea";
        $http.get(url)
            .success(function(data) {

                tabla_Personas.save(data.results, function(_data) {
                    console.log('New items:', data.results);
                });
                $window.location.reload();
            })
            .error(function(err) {
                console.log("Error al consumir randomapi.com");
                alert("Error al consumir randomapi.com")
                console.log(data);
                // alert(data);
            });
    };

});


function showModal() {
    var modal = document.querySelector('ons-modal');
    modal.show();
    setTimeout(function() {
        modal.hide();
    }, 2000);
}



if (debug) {
    $(document).ready(function() {
        angular.bootstrap($("#theapp"), ['app']);
    });
} else {
    document.addEventListener('deviceready', function() {
        angular.bootstrap($("#theapp"), ['app']);
    });
}